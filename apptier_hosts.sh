#!/bin/bash
# Sets hostname using $cliqrNodeId 
# Sets ip's & hostnames in /etc/hosts - uses all app tier dependents and dependencies
# Tested on CentOS 7


cliqr_userenv_file='/usr/local/osmosix/etc/userenv'

echo "INFO: Setting environment variables from: '${cliqr_userenv_file}'"
. $cliqr_userenv_file

if [[ ! -z "${domain// }" ]]; then
  domain=".$domain"
fi
line_one='127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4'

fqdn="${cliqrNodeId}${domain}"

fqdn="${fqdn,,}"

echo "INFO: Setting my hostname as ${fqdn}"
sudo echo -e "$line_one\r\n$(hostname -I | cut -d" " -f 1) $fqdn $(echo $fqdn | cut -f1 -d".")" | sudo tee /etc/hosts && sudo hostnamectl set-hostname "${cliqrNodeId}${domain}"

echo "INFO: Setting hostnames in /etc/hosts using this server's dependents from the CliqrDependents list: $CliqrDependents"
IFS=',' read -a CliqrDependentsArray <<< "$CliqrDependents"

for dep in $CliqrDependentsArray; do
  dep_ips=$(eval echo \$$"CliqrTier_${dep}_IP")
  IFS=',' read -a tier_ips_array <<< "$dep_ips"
  dep_hostnames=$(eval echo \$"CliqrTier_${dep}_NODE_ID")
  IFS=',' read -a tier_hostnames_array <<< "$dep_hostnames"
  index=0
  for ip in $tier_ips_array; do
    host_fqdn="${tier_hostnames_array[$index]}${domain}"
    host_fqdn="${host_fqdn,,}"
    echo "Setting host entry in /etc/hosts: IP:$ip FQDN:$host_fqdn"
    sudo echo -e "$ip $host_fqdn $(echo $host_fqdn | cut -f1 -d".")" | sudo tee --append /etc/hosts
    index=$((index + 1))
  done
done

echo "INFO: Setting hostnames in /etc/hosts using this server's dependencies from the CliqrDependencies list: $CliqrDependencies"
IFS=',' read -a CliqrDependenciesArray <<< "$CliqrDependencies"

for dep in $CliqrDependenciesArray; do
  dep_ips=$(eval echo \$$"CliqrTier_${dep}_IP")
  IFS=',' read -a tier_ips_array <<< "$dep_ips"
  dep_hostnames=$(eval echo \$"CliqrTier_${dep}_NODE_ID")
  IFS=',' read -a tier_hostnames_array <<< "$dep_hostnames"
  index=0
  for ip in $tier_ips_array; do
    host_fqdn="${tier_hostnames_array[$index]}${domain}"
    host_fqdn="${host_fqdn,,}"
    echo "Setting host entry in /etc/hosts: IP:$ip Host:$host_fqdn"
    sudo echo -e "$ip $host_fqdn $(echo $host_fqdn | cut -f1 -d".")" | sudo tee --append /etc/hosts
    index=$((index + 1))
  done
done