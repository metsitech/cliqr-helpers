#!/bin/bash
# Sets hostname using $cliqrNodeId and optional arg ($1) as domain 
# Tested on CentOS 7

cliqr_userenv_file='/usr/local/osmosix/etc/userenv'

echo "INFO: Setting environment variables from: '${cliqr_userenv_file}'"
. $cliqr_userenv_file

line_one='127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4'

domain=$1
if [[ ! -z "${domain// }" ]]; then
  domain=".$1"
fi

lowercase_nodeid="${cliqrNodeId,,}"

new_fqdn="${lowercase_nodeid}${domain}"
echo "Setting my hostname to: ${new_fqdn}"
sudo echo -e "$line_one\r\n$(hostname -I | cut -d" " -f 1) $new_fqdn $(echo $new_fqdn | cut -f1 -d".")" | sudo tee /etc/hosts && sudo hostnamectl set-hostname "${new_fqdn}"
