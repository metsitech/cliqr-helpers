#!/bin/bash
# Sets hostname using $cliqrAppTierName 
# Sets hosts in /etc/hosts - uses all app tier dependents and dependencies
# Tested on CentOS 7

cliqr_userenv_file='/usr/local/osmosix/etc/userenv'

echo "INFO: Setting environment variables from: '${cliqr_userenv_file}'"
. $cliqr_userenv_file

echo "INFO: Setting my hostname as ${cliqrAppTierName}"
sudo echo -e "$(hostname -I | cut -d" " -f 1) ${cliqrAppTierName}" | sudo tee --append /etc/hosts && sudo hostnamectl set-hostname $cliqrAppTierName

echo "INFO: Setting hostnames in /etc/hosts using this server's dependents from the CliqrDependents list: $CliqrDependents"
IFS=',' read -a CliqrDependentsList <<< "$CliqrDependents"

for dep in $CliqrDependentsList
do
  dep_ip=$(eval echo \$$"CliqrTier_${dep}_IP")
  echo "Setting host entry in /etc/hosts: IP:$dep_ip Host:$dep"
  sudo echo -e "$dep_ip $dep" | sudo tee --append /etc/hosts
done

echo "INFO: Setting hostnames in /etc/hosts using this server's dependencies from the CliqrDependencies list: $CliqrDependencies"
IFS=',' read -a CliqrDependenciesList <<< "$CliqrDependencies"

for dep in $CliqrDependenciesList
do
  dep_ip=$(eval echo \$$"CliqrTier_${dep}_IP")
  echo "Setting host entry in /etc/hosts: IP:$dep_ip Host:$dep"
  sudo echo -e "$dep_ip $dep" | sudo tee --append /etc/hosts
done