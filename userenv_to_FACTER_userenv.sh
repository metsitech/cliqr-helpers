#!/bin/bash
# Creates 'FACTER_userenv' to enable all Cloudcenter environment variables to be exported also as FACTER variables.

userenv_path=/usr/local/osmosix/etc
sed -e 's/export /export FACTER_/g' "${userenv_path}/userenv" > "${userenv_path}/FACTER_userenv"

