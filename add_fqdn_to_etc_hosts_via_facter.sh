#!/bin/bash
# Installs facter & adds a line to the top of /etc/hosts (using facter to resolve) to ensure hostname and fqdn is resolvable.
# Tested on CentOS 7

echo "$(date) Installing facter..."
yum install epel-release -y
yum -y install facter

export PATH=$PATH:/usr/local/bin

my_ip=$(facter ipaddress) 
my_fqdn=$(facter fqdn)
my_hostname=$(facter hostname)

echo "$(date) Adding:[$my_ip $my_fqdn $my_hostname] to /etc/hosts..."
sudo echo -e "$my_ip $my_fqdn $my_hostname\r\n$(cat /etc/hosts)" | sudo tee /etc/hosts 